import { Link } from 'react-router-dom';
import '../Gallery/GalleryHouse.scss';


export function GalleryHouses(props){
    const houses = props.housesList;
    return(
        <div className="c-houses">
            <div className="row ">
                {houses.map((item,i)=>
                <div className="col-12 col-md-sm6 col-md-4 col-lg-2"  key={i}>
                    <Link to={"/houses/"+ item.name}>
                        <div className="flag">
                            <div className="flag__img">
                                {item.logoURL ? <img src={item.logoURL} alt =""/> : 
                                <img src="https://spoilertime.com/wp-content/uploads/2017/12/House-Stark-Main-Shield.png" alt =""/> }
                            </div>
                            <div className="flag__text">
                                <p>{item.name}</p>
                            </div>
                    
                        </div>
                    </Link>
                </div> 
                )}
            </div>   
        </div>
)};

