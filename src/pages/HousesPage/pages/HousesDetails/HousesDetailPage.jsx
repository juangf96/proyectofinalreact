import { useParams } from "react-router";
import axios from 'axios';
import { useEffect, useState } from "react";
import './HousesDetailPage.scss';
import { NavLink } from "react-router-dom";

export function HousesDetailPage(){
    const[houses,setHouses]= useState([])

    
    const { namehouse }= useParams();

    const getHouses = ()=>{
        axios('https://api.got.show/api/show/houses/'+ namehouse).then(res=>{
            console.log(namehouse)
            console.log(res.data)
            setHouses(res.data[0])
            
         });
    }

    
    useEffect(()=>getHouses(),[])

   

return(
    <div>
    <NavLink exact to="/houses">
        <span className="icon-arrow-left"><p>Volver</p></span></NavLink>
    <div className="houseDetail">
        <div className="flags">
            
            {houses.logoURL ? <img className="flags__img" src={houses.logoURL} alt="houseimg"/> : 
            <img className="flags__img" src="https://spoilertime.com/wp-content/uploads/2017/12/House-Stark-Main-Shield.png" alt="imagen"/> }

            <p className="flags__text">{houses.name}</p>
        </div>
        <div className="list">
            <div className="list__words">
                <h2>LEMA</h2>
                <p>{houses.words}</p>
            </div>
            <div className="list__seat">
                <h2>SEDE</h2>
                <p>{houses.seat}</p>
            </div>
            <div className="list__region">
                <h2>REGION</h2>
                <p>{houses.region}</p>
            </div>
            <div className="list__allegiance">
                <h2>ALIANZAS</h2>
                {Array.isArray(houses.allegiance) ? houses.allegiance.map( (item,i)=>
                <p key={i}>{item}</p>) : <p>{houses.allegiance}</p> }
            </div>
            <div className="list__religion">
                <h2>RELIGIONES</h2>
                {Array.isArray(houses.religion) ? houses.religion.map( (item,i)=>
                <p key={i}>{item}</p>) : <p>{houses.religion}</p> }
            </div>
            <div className="list__createdAt">
                <h2>FUNDACION</h2>
                <p>{houses.createdAt}</p>
            </div>
        </div>
        
        
    </div>
    </div>

    )
}
