import { useParams } from 'react-router';
import axios from 'axios';
import { useEffect, useState } from 'react';
import './CharacterDetailPage.scss';
import { NavLink } from 'react-router-dom';

let arrayAllegiances = [];
export function CharactersDetailPage() {
	const [ character, setCharacters ] = useState([]);

	const [ houses, setHouses ] = useState([]);

	const { name } = useParams();

	const getCharacter = () => {
		axios('https://api.got.show/api/show/characters/' + name).then((res) => {
			console.log(name);
			console.log(res.data);
			setCharacters(res.data);
			console.log(character.allegiances);
			arrayAllegiances = character.allegiances;
		});
	};

	const getHouses = () => {
		axios('https://api.got.show/api/show/houses/' + character.house).then((res) => {
			if (res.data[0] != undefined) {
				setHouses(res.data[0]);
				console.log(character.house);
				console.log(res.data[0]);
			}
		});
	};
	
	useEffect(() => getCharacter(), []);

	useEffect(() => getHouses(), [ character.house ]);

	return (
		<div>
		<NavLink exact to="/characters">
        <span className="icon-arrow-left"><p>Volver</p></span></NavLink>
		<div className="c-detail">
			
			
			
                <div className="character">
					<img className="character__img" src={character.image} alt="img" />
					<p className="character__text">{character.name}</p>
                </div>
			
			
			<div  className="list">
				<div className="list__house">
					<h2>CASAS</h2>
					<img src={houses.logoURL} />
				</div>
				<div className="list__allegiances">
					<h2>ALIANZAS</h2>
					{Array.isArray(character.allegiances) ? (
						character.allegiances.map((item, i) => <p key={i}>{item}</p>)
					) : (
						<p>{character.allegiances}</p>
					)}
				</div>
				<div className="list__appearances">
					<h2>APARICIONES</h2>
                    <div className="scroll">
					    {Array.isArray(character.appearances) ? (
						character.appearances.map((item, i) => <p key={i}>{item}</p>)
					    ) : (
						<p>{character.appearances}</p>
					    )}
                    </div>
				</div>
                <div className="list__father">
				    <h2>PADRE</h2>
				    <p>{character.father}</p>
			    </div>
                <div className="list__siblings">
				    <h2>DESCENDIENTES</h2>
				    {Array.isArray(character.siblings) ? (
					character.siblings.map((item, i) => <p key={i}>{item}</p>)
				    ) : (
					<p>{character.siblings}</p>
				    )}
			    </div>
                <div className="list__titles">
				    <h2>TITULOS</h2>
                    <div className="scroll">
					
				        {Array.isArray(character.titles) ? (
					    character.titles.map((item, i) => <p key={i}>{item}</p>)
				        ) : (
					    <p>{character.titles}</p>
				        )}
					
                    </div>
			    </div>


			</div>

			
			
			
		</div>
		</div>
       
	);
}
