import { useEffect, useState } from "react"
import axios from 'axios';
import { Gallery } from "./pages/Gallery/Gallery";
import { SearchForm } from "../../shared/SearchForm/SearchForm";
import { Navbar } from "../../core/Navbar/Navbar";



let allCharacters = [];
export function CharactersPage(){
    const[characters,setCharacters]= useState([]);


    const getCharacters = ()=>{
        axios('https://api.got.show/api/show/characters/' ).then(res=>{
            allCharacters = res.data
            console.log(res.data)
            setCharacters(res.data)
         });
    }

    // const getCharactersFilter =(characterFiltered)=>{
    //     axios('https://api.got.show/api/show/characters/'+characterFiltered).then(res=>{
    //         console.log(res.data)
    //         setCharacters(res.data)
    //     });
    // }
    const filterCharacters = (filterCharactersName)=>{
        const characteresFiltered= allCharacters.filter(characters =>
            characters.name.toLowerCase().includes(filterCharactersName.toLowerCase()));
        setCharacters(characteresFiltered);
    }


    useEffect(getCharacters,[])
    return(
        <div>
            <SearchForm fnFilter={filterCharacters}/>
            <Gallery charactersList={characters}/>
            <Navbar/>
        </div>
    )
}
